<?php
// This is the file which is used to display the settings page in the admin panel
namespace OCA\ShareMover\Settings;

use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Services\IInitialState;
use OCP\Settings\ISettings;
use OCP\IUserSession;
use OCA\ShareMover\ConfigService;
use OCA\ShareMover\AppInfo\Application;

class User implements ISettings {

	public function __construct(
		private ConfigService $configService,
		private IInitialState $initialStateService,
		private IUserSession $userSession
	) {
	}

	// Mandadory method for ISettings, sets the default values and returns the template form
	/**
	 * @inheritDoc
	 */
	public function getForm(): TemplateResponse {
		// Fetch the currently logged-in user
        $user = $this->userSession->getUser();
        if ($user === null) {
            throw new \Exception("No user session found");
        }
        $userId = $user->getUID();

        // Fetch user-specific settings
        $userConfigMimeTypes = $this->configService->getUserConfigMimeTypes($userId);
        $userConfigDestinationsReadOnly = $this->configService->getUserConfigDestinationsReadOnly($userId);
        $userConfigDestinations = $this->configService->getUserConfigDestinations($userId);

		// Prepare initial state for the Vue component
        $userConfig = [
            'mimeTypes' => $userConfigMimeTypes,
            'destinationsReadOnly' => $userConfigDestinationsReadOnly,
            'destinations' => $userConfigDestinations
        ];
        $this->initialStateService->provideInitialState('user-config', $userConfig);

		// Create a new TemplateResponse, pass the app-id and the template file name
		return new TemplateResponse(Application::APP_ID, 'userSettings'); 
	}

	// Get the section that those app settings should be displayed in
	/**
	 * @inheritDoc
	 */
	public function getSection(): string {
		return 'sharing';
	}

	// Where to place the content on the page
	/**
	 * @inheritDoc
	 */
	public function getPriority(): int {
		return 90;
	}
}
