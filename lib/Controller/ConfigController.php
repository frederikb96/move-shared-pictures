<?php
// Inherits from the Controller class and manages the admin and user config values
// It directly access the Nextcloud database functionalities
namespace OCA\ShareMover\Controller;

use OCP\IConfig;
use OCP\IRequest;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Http;
use OCP\AppFramework\Http\Attribute\NoAdminRequired;

use OCA\ShareMover\AppInfo\Application;

class ConfigController extends Controller {

	public function __construct(
		string $appName,
		IRequest $request,
		private IConfig $config,
		private ?string $userId
	) {
		parent::__construct($appName, $request);
	}

	/**
	 * Set admin config values
	 *
	 * @param array $values key/value pairs to store in app config
	 * @return DataResponse
	 */
	public function setAdminConfig(array $values): DataResponse {
		// Set the values in the database and is called from the frontend via the routes.php file
		foreach ($values as $key => $value) {
			$this->config->setAppValue(Application::APP_ID, $key, $value);
		}
		return new DataResponse(['status' => 'success']);
	}

	/**
     * Set user config values
     *
     * @param array $values key/value pairs to store in user config
     * @return DataResponse
     */
	#[NoAdminRequired]
    public function setUserConfig(array $values): DataResponse {
        // Ensure the current user is authorized to modify settings
        if ($this->userId !== null) {
            foreach ($values as $key => $value) {
                // Set the values for the specific user
                $this->config->setUserValue($this->userId, Application::APP_ID, $key, $value);
            }
            return new DataResponse(['status' => 'success']);
        }
        return new DataResponse(['status' => 'error', 'message' => 'Unauthorized'], Http::STATUS_UNAUTHORIZED);
    }
}