<?php
declare(strict_types=1);
// SPDX-FileCopyrightText: Frederik Berg
// SPDX-License-Identifier: AGPL-3.0-or-later

namespace OCA\ShareMover\Listeners;

use OC\User\NoUserException;
use OCA\Circles\CirclesManager;
use OCA\Circles\Exceptions\CircleNotFoundException;
use OCA\Circles\Exceptions\InitiatorNotFoundException;
use OCA\Circles\Exceptions\RequestBuilderException;
use OCP\Files\File;
use OCP\Files\IRootFolder;
use OCP\Files\NotFoundException;
use OCP\Files\NotPermittedException;
use OCP\IGroupManager as IGroupManager;
use OCP\Share\IManager as IShareManager;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;
use OCP\Share\Events\ShareCreatedEvent;
use OCP\Share\IShare;
use OCA\ShareMover\ConfigService;
use Psr\Log\LoggerInterface;


class ShareCreatedListener implements IEventListener {
	/** @var LoggerInterface */
	private LoggerInterface $logger;
	/** @var IShareManager */
	private IShareManager $shareManager;
	/** @var IGroupManager */
	private IGroupManager $groupManager;
	/** @var CirclesManager */
	private CirclesManager $circlesManager;
	/** @var IRootFolder */
	private IRootFolder $rootFolder;
	/** @var ConfigService */
	private ConfigService $configService;

	public function __construct(LoggerInterface $logger,
								IShareManager $shareManager,
								IGroupManager $groupManager,
								CirclesManager $circlesManager,
								IRootFolder $rootFolder,
								ConfigService $configService) {
		$this->logger = $logger;
		$this->shareManager = $shareManager;
		$this->groupManager = $groupManager;
		$this->circlesManager = $circlesManager;
		$this->rootFolder = $rootFolder;
		$this->configService = $configService;
	}

	/**
	 * Joins two file path strings with a single slash between them.
	 *
	 * This function ensures that there is exactly one slash between the two paths,
	 * regardless of whether the first path ends with a slash or the second path
	 * starts with a slash.
	 *
	 * @param string $path1 The first path segment.
	 * @param string $path2 The second path segment.
	 * @return string The joined path.
	 */
	private function joinPaths(string $path1, string $path2): string {
		// Remove any trailing slash from the first path and any leading slash from the second path
		$path1 = rtrim($path1, '/') . '/';
		$path2 = ltrim($path2, '/');

		// Concatenate the two paths with a single slash between them
		return $path1 . $path2;
	}

	/**
	 * Checks for a file if it matches one of the mimetypes
	 *
	 * @param File $file
	 * @param array $mimeTypes
	 * @return array Returns array with positions in mimetypes array that match
	 */
	private function checkMimeTypeFile(File $file, array $mimeTypes): array {
		// Empty array which will store the positions that matches in the array
		$positions = [];

		$fileType = $file->getMimeType();
		// Iterate over mime type list and check if matches
		$position = 0;
		foreach ($mimeTypes as $mimeType) {
			if (str_contains($fileType, $mimeType)) {
				$positions[] = $position; // Add position to positions array
			}
			$position++;
		}
		return $positions;
	}

	/**
	 * Checks for a share if its files match one of the mimetypes.
	 * Uses the first files mimetype in folders that match.
	 *
	 * @param IShare $share
	 * @param array $mimeTypes
	 * @return array Returns array with positions in mimetypes array that match
	 */
	private function checkMimeTypeFolderFile(IShare $share, array $mimeTypes): array {

		// Empty positions array as default return
		$positions = [];

		// Check if share is file or folder
		try {
			$shareNodeType = $share->getNodeType();
		} catch (NotFoundException $e) {
			$this->logger->error('Cannot get node type of share!', ['exception' => $e,]);
			return $positions;
		}

		try {
			$shareNode = $share->getNode();
		} catch (NotFoundException $e) {
			$this->logger->error('Cannot get node of share!', ['exception' => $e,]);
			return $positions;
		}

		if ($shareNodeType === 'file') {
			// if file directly call on it
			$positions = $this->checkMimeTypeFile($shareNode, $mimeTypes);
		} else {
			// else get folder and its files and check for each file
			$files = $shareNode->getDirectoryListing();

			// Iterate over all files in folder and check if there is a correct mime type in there
			foreach ($files as $file) {
				$positions = $this->checkMimeTypeFile($file, $mimeTypes);
				if (!empty($positions)) {
					// Take first non-empty one
					break;
				}
			}
		}
		return $positions;
	}

	/**
	 * For a share and a user, it checks which config to use and returns the position array and a flag if user or admin config.
	 *
	 * @param IShare $share
	 * @param string $user
	 * @return array Returns a 2 dim array, 1 entry array with position array and second a flag true if user
	 */
	private function checkMimeTypeUserAdmin(IShare $share, string $user): array {
		// default values for return
		$positions = [];
		$configUser = False;

		// Get the mimeTypes supported by user
		$mimeTypes = $this->configService->getUserConfigMimeTypes($user);
		if (!empty($mimeTypes)) {
			$configUser = True;

		} else {
			// No user settings, lets use admin once
			$mimeTypes = $this->configService->getConfigMimeTypes();
		}
		// We get the array containing the mimetypes and check it together with the share
		$positions = $this->checkMimeTypeFolderFile($share, $mimeTypes);

		return [$positions, $configUser];
	}

	/**
	 * Check if one of the folders specified in the path exist for user
	 *
	 * @param string $user
	 * @param array $paths
	 * @return string Return the path to the destination folder, "" if none
	 */
	private function findFolderExisting(string $user, array $paths): string {
		// Return empty, if not found
		$destination = "";

		// Get user root folder from user id
		try {
			$userFolder = $this->rootFolder->getUserFolder($user);
		} catch (NotPermittedException $e) {
			$this->logger->error('Not permitted to get root folder of user ' . $user, ['exception' => $e,]);
			return $destination;
		} catch (NoUserException $e) {
			$this->logger->error('User' . $user . 'not found!', ['exception' => $e,]);
			return $destination;
		}

		// Check for all possible destination folders if one exists
		foreach($paths as $path) {
			if ($userFolder->nodeExists($path)) {
				$destination = $path;
				break;
			}
		}
		// Return first found
		return $destination;
	}

	/**
	 * Takes a share, user, and path array and moves the share to the first available path
	 *
	 * @param IShare $share
	 * @param string $user
	 * @param array $pathsDest
	 * @return bool $moved
	 */
	private function moveUserShareToPath(IShare $share, string $user, array $pathsDest): bool {
		// default values for return
		$moved = False;

		$pathDest = $this->findFolderExisting($user, $pathsDest);

		if (!empty($pathDest)) {
			// A folder is available for user so move it to this

			// Get folder name from share
			$folderName = basename($share->getTarget());

			// Set target of share to new destination
			$share->setTarget($this->joinPaths($pathDest, $folderName));
			try {
				$this->shareManager->moveShare($share, $user);
				$this->logger->info('Moved ' . $folderName . ' to ' . $pathDest);
				$moved = True;
			} catch (\InvalidArgumentException $e) {
				$this->logger->error('Not possible to move ' . $folderName . ' to ' . $pathDest, ['exception' => $e,]);
			}
		}

		return $moved;
	}

	/**
	 * Move the share the user received to a location configured in the config
	 *
	 * @param IShare $share
	 * @param string $user
	 * @return bool Returns true if it was possible to move the share
	 */
	private function moveUserShare(IShare $share, string $user): bool {

		// Default values for return
		$moved = False;

		// Check which config to use
		$checkMimeTypeUserAdmin = $this->checkMimeTypeUserAdmin($share, $user);
		// Check if it is empty
		$positions = $checkMimeTypeUserAdmin[0];
		if (empty($positions)) {
			// No mime type match, so we are done
			return $moved;
		}

		// Check if user or admin config
		$configUser = $checkMimeTypeUserAdmin[1];

		// Check if read only share and return after done
		if ($share->getPermissions() === \OCP\Constants::PERMISSION_READ) {

			// Check if admin or user config shall be used
			if ($configUser) {
				// Get values from user config
				$pathDest = $this->configService->getUserConfigDestinationsReadOnly($user);
			} else {
				// Get values from global config
				$pathsDest = $this->configService->getConfigDestinationsReadOnly();
			}

		} else {

			// Check if admin or user config shall be used
			if ($configUser) {
				// Get values from user config
				$pathsDest = $this->configService->getUserConfigDestinations($user);
			} else {
				// Get values from global config
				$pathsDest = $this->configService->getConfigDestinations();
			}
		}

		// We only want the entries in the pathsDest array that match the index positions in the positions array
		$pathsDest = array_intersect_key($pathsDest, array_flip($positions));

		// Move to the new destination
		$moved = $this->moveUserShareToPath($share, $user, $pathsDest);

		return $moved;
	}

	public function handle(Event $event): void {

		// Check if app is enabled
		$appStatus = $this->configService->getAppStatus();

		if ($appStatus) {
			$this->logger->info('App is enabled!');
		} else {
			$this->logger->info('App is disabled!');
			return;
		}

		// Check if event is a share event, should be the case always
		if (!($event instanceof ShareCreatedEvent)) {
			$this->logger->error("Not a share event!");
			return;
		}

		// Get the share, should be possible always
		try {
			$share = $event->getShare();
		} catch (NotFoundException $e) {
			$this->logger->error('Cannot get share!', ['exception' => $e,]);
			return;
		}

		// Get Share Receiver
		try {
			$shareReceiver = $share->getSharedWith();
		} catch (NotFoundException $e) {
			$this->logger->error('Cannot get share receiver!', ['exception' => $e,]);
			return;
		}

		// Get share type
		try {
			$shareType = $share->getShareType();
		} catch (NotFoundException $e) {
			$this->logger->error('Cannot get share type!', ['exception' => $e,]);
			return;
		}

		// User vs Group
		if ($shareType === IShare::TYPE_USER) {
			// User
			$this->moveUserShare($share, $shareReceiver);
		} elseif ($shareType === IShare::TYPE_GROUP) {
			// Group
			// Get members
			try {
				$members = $this->groupManager->displayNamesInGroup($shareReceiver);
			} catch (NotFoundException $e) {
				$this->logger->error('Cannot get members of group!', ['exception' => $e,]);
				return;
			}
			// Do move for all members
			foreach($members as $id) {
				// Skip if it is the owner of the share
				if ($id === $share->getShareOwner()) {
					continue;
				}
				$this->moveUserShare($share, $id);
			}
		} elseif ($shareType === IShare::TYPE_CIRCLE) {
			// Circle
			try {
				$circle = $this->circlesManager->getCircle($shareReceiver);
			} catch (CircleNotFoundException $e) {
				$this->logger->error('Circle not found!', ['exception' => $e,]);
				return;
			} catch (InitiatorNotFoundException $e) {
				$this->logger->error('Circle initiator not found!', ['exception' => $e,]);
				return;
			} catch (RequestBuilderException $e) {
				$this->logger->error('Circle request build not possible!', ['exception' => $e,]);
				return;
			}
			try {
				$members = $circle->getMembers();
			} catch (NotFoundException $e) {
				$this->logger->error('Cannot get members of circle!', ['exception' => $e,]);
				return;
			}
			// Do move for all members
			foreach($members as $member) {
				$id = $member->getUserId();
				// Skip if it is the owner of the share
				if ($id === $share->getShareOwner()) {
					continue;
				}
				$this->moveUserShare($share, $id);
			}
		}
	}
}
