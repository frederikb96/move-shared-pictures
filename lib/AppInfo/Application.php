<?php
declare(strict_types=1);
// SPDX-FileCopyrightText: Frederik Berg
// SPDX-License-Identifier: AGPL-3.0-or-later

namespace OCA\ShareMover\AppInfo;

use OCA\ShareMover\Listeners\ShareCreatedListener;
use OCP\AppFramework\App;
use OCP\AppFramework\Bootstrap\IBootContext;
use OCP\AppFramework\Bootstrap\IBootstrap;
use OCP\AppFramework\Bootstrap\IRegistrationContext;
use OCP\Share\Events\ShareCreatedEvent;

class Application extends App implements IBootstrap {
	public const APP_ID = 'sharemover';

	public function __construct() {
		parent::__construct(self::APP_ID);
	}

	public function register(IRegistrationContext $context): void {
		$context->registerEventListener(ShareCreatedEvent::class, ShareCreatedListener::class);
	}

    public function boot(IBootContext $context): void {
	}
}
