<?php
// This file is used to read the config values from the database which nextcloud provides
// So it basically provides helper functions
// ConfigController.php stores the values in the database and this file reads them
namespace OCA\ShareMover;

use OCP\IConfig;
use OCA\ShareMover\AppInfo\Application;

class ConfigService {

    const APP_ID = Application::APP_ID;

    private $config;

    public function __construct(IConfig $config) {
        $this->config = $config;
    }

    public function getAppStatus(): bool {
        // Simply get the value which is either 0 or 1 since the config value is a checkbox which is either checked or not and return true if it is 1
        return $this->config->getAppValue(self::APP_ID, 'appStatus', "0") === '1';
    }

    public function getConfigMimeTypes(): array {
        $value = $this->config->getAppValue(self::APP_ID, 'mimeTypes', "[]");
        $decodedValue = json_decode($value, true);
    
        // Check if the decoded value is an array; if not, return an empty array.
        return is_array($decodedValue) ? $decodedValue : [];
    }
    
    public function getConfigDestinationsReadOnly(): array {
        $value = $this->config->getAppValue(self::APP_ID, 'destinationsReadOnly', "[]");
        $decodedValue = json_decode($value, true);
    
        // Check if the decoded value is an array; if not, return an empty array.
        return is_array($decodedValue) ? $decodedValue : [];
    }
    
    public function getConfigDestinations(): array {
        $value = $this->config->getAppValue(self::APP_ID, 'destinations', "[]");
        $decodedValue = json_decode($value, true);
    
        // Check if the decoded value is an array; if not, return an empty array.
        return is_array($decodedValue) ? $decodedValue : [];
    }

    public function getUserConfigMimeTypes(string $userId): array {
        $value = $this->config->getUserValue($userId, self::APP_ID, 'mimeTypes', "[]");
        $decodedValue = json_decode($value, true);
    
        // Check if the decoded value is an array; if not, return an empty array.
        return is_array($decodedValue) ? $decodedValue : [];
    }
    
    public function getUserConfigDestinationsReadOnly(string $userId): array {
        $value = $this->config->getUserValue($userId, self::APP_ID, 'destinationsReadOnly', "[]");
        $decodedValue = json_decode($value, true);
    
        // Check if the decoded value is an array; if not, return an empty array.
        return is_array($decodedValue) ? $decodedValue : [];
    }
    
    public function getUserConfigDestinations(string $userId): array {
        $value = $this->config->getUserValue($userId, self::APP_ID, 'destinations', "[]");
        $decodedValue = json_decode($value, true);
    
        // Check if the decoded value is an array; if not, return an empty array.
        return is_array($decodedValue) ? $decodedValue : [];
    }
    
}
