# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.3.1] - 2023-12-20

### Added

- Nextcloud Store release.

## [1.3.0] - 2023-12-18

### Added

- Support for Nextcloud 28.

### Removed

- Rmoved deprecated permission change checkbox in the admin settings such that permissions are not manupulated anymore.

## [1.2.1] - 2023-12-16

### Added

- This changelog.

### Changed

- Releases are only triggerd based on keywords in the commit message.
- Build CI is also triggered by merge requests.

## [1.2.0] - 2023-12-15

### Added

- Releases are now automatically built and uploaded to the [Releases](https://gitlab.com/frederikb96/nextcloud-mvsharedpics/-/releases) page.

## [1.1.0] - 2023-12-14

### Fixed

- Fixed errors about permission change checkbox in the admin settings.
- Improved some other parts of the code and descriptions.

## [1.0.0] - 2023-12-14

### Changed

- First version with GUI support for admin and user settings.

## [0.0.1] - 2023-04-16

### Added

- First functional version of the app with config per xml Nextcloud config file.