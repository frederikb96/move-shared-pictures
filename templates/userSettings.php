<?php
$appId = OCA\ShareMover\AppInfo\Application::APP_ID;
\OCP\Util::addScript($appId, $appId . '-userSettings');
// loads the script, which should be attached to the empty div
?>

<div id="sharemover_user_settings"></div>
