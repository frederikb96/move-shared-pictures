<!--
SPDX-FileCopyrightText: Frederik Berg
SPDX-License-Identifier: CC0-1.0
-->

# Nextcloud ShareMover
[![pipeline status](https://gitlab.com/frederikb96/nextcloud-mvsharedpics/badges/main/pipeline.svg)](https://gitlab.com/frederikb96/nextcloud-mvsharedpics/-/commits/main) [![Latest Release](https://gitlab.com/frederikb96/nextcloud-mvsharedpics/-/badges/release.svg)](https://gitlab.com/frederikb96/nextcloud-mvsharedpics/-/releases)

This is a [Nextcloud server app](https://apps.nextcloud.com/apps/sharemover), which can be installed by administrators of a Nextcloud instance.

If you receive a shared file/folder within Nextcloud, the file/folder will normally be placed in the standard directory for shared files. This App automatically moves shared files/folders, which have/contain a specific mime type (like pictures), to another customizable directory. This way, you can automatically sort your shared files/folders into different directories. After installing the app, you can adjust system wide settings as the admin or personal settings as a user in the Sharing section of Nextcloud Settings.

## Disclaimer
Use this app at your own risk. I am using this app currently with ~7 persons on my Nextcloud instance and everything works fine. But there is no guarantee. I am not responsible for any data loss or other damage caused by the use of this app. If you find any bugs or have feature requests, please open an issue on the [Gitlab page](https://gitlab.com/frederikb96/nextcloud-mvsharedpics/-/issues).

## Install Store
Just install the [app](https://apps.nextcloud.com/apps/sharemover) as an Admin via the Nextcloud App Store. And configure your desired settings in the Settings GUI of Nextcloud in the Sharing section.

## Install Manually
1. Go to [Releases](https://gitlab.com/frederikb96/nextcloud-mvsharedpics/-/releases) and download the latest release package as a tar archive.
2. Extract the archive 'tar -xf sharemover*.tar.gz'
3. Copy the content of the extracted folder to a folder named `sharemover` in your `custom_apps` (maybe different name) directory, which is in your Nextcloud server root directory.
4. Change the owner of the sharemover folder to www-data (or whatever user your webserver is running with) `chown -R www-data:www-data sharemover`
5. In the browser, log in with an admin account to your Nextcloud instance and go to the 'Apps' menu. Now search for `ShareMover` and enable the app.
6. Configure your desired settings in the Settings GUI of Nextcloud in the Sharing section.

## Les Pas Workflow
I personally developed the app to enable a specific workflow together with the Les Pas Android app to use it with my family.
- For every "joint" share, the shared folder gets moved to the `lespas` folder of every user, that is part of the share. If the user does not have a `lespas` folder, it gets moved to the normal `Photos` folder. This way, the users that received the joint share, have full control over the shared folder and can access it in their Les Pas App as it would be their own folder. This is nice for family albums, where everyone should be able to add new pictures and see those in the primary Les Pas view.
- For every "solo" share (read-only), the shared folder is simply moved to a `lespas_read_only` folder. This way the share is only visible in the normal "share" tab of the Les Pas app but the folder is still in a designated folder and not in the normal folder for shares.

Thus, my admin config looks basically like this:
```
image, image, image, image, image, image, image, image
/Photos/lespas_read_only, /Pictures/lespas_read_only, /Fotos/lespas_read_only, /Bilder/lespas_read_only, /Photos, /Pictures, /Fotos, /Bilder
/Photos/lespas, /Pictures/lespas, /Fotos/lespas, /Bilder/lespas, /Photos, /Pictures, /Fotos, /Bilder
```