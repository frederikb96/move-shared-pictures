.PHONY: all nstall ci build dev watch clean

# Default production
all: clean install build

# Default Dev
all-dev: clean install dev

# Install dependencies
install:
	npm install

# Install dependencies in a faster way, not for development
ci:
	npm ci

# Build for production
build:
	npm run build

# Build for development
dev:
	npm run dev

# Watches for changes and rebuilds automatically in development
watch:
	npm run watch

# Clean up to get initial state
clean:
	rm -rf node_modules
	rm -rf js
